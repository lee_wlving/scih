function [Ex, Ey,C,M,  B,Px,Py] = train(X, Y, param, L)

fprintf('training...\n');

%% set the parameters
nbits = param.nbits;
lambda = param.lambda;
alpha = param.alpha;
gamma = param.gamma;
mu = param.mu;
beta=param.beta;
lambdaE=1;

%% get the dimensions
[n, dX] = size(X);
dY = size(Y,2);

%% transpose the matrices
X = X'; Y = Y'; L = L';

%% initialization
V = randn(nbits, n);
Ex = randn(nbits, dX);
Ey = randn(nbits, dY);
C=randn(nbits, dY);
Px=randn(nbits,dX);
Py=Px;
R = randn(nbits, nbits);
[U11, ~, ~] = svd(R);
B=randn(nbits,n);

M = U11(:,1:nbits);
S=L'*L;

%% iterative optimization
for iter = 1:param.iter
    
    J1=V-Ex*X;
    J2=V-Ey*Y;
    K1=V-C*X;
    K2=V-C*Y;
    G1=(C+Ex)*X;
    G2=(C+Ey)*Y;
    
    
    % update C
    C=(lambda*J1*X' + (1-lambda)*J2*Y') *inv(lambda*X*X' + (1-lambda)*Y*Y'+mu*eye(dY));

    % update E
    Ex = (2*lambda*K1*X' -alpha* Ey) *inv(2*lambda*X*X' +2*mu*eye(dX));
    Ey = (2*(1-lambda)*K2*Y' -alpha*Ex) *inv(2*(1-lambda)*Y*Y'+2*mu*eye(dX));
    

    % update V
    %fprintf('%d\n',size(S,2));
    V=inv(eye(nbits)+gamma*M'*M+beta*M'*B*B'*M)*(lambda*G1+(1-lambda)*G2+ gamma*M'*B+beta*M'*B*S) ;

    % update B
    B=inv(gamma*eye(nbits)+beta*M*V * V'*M')*(gamma*M*V +beta* M * V*S')  ;
    
    %update W
    
    
    
    % update M
    [S1, ~, S2] = svd(gamma*V*B'+beta*V*S'*B');
    R = S1*S2';
    %fprintf('%d\n',size(X,2));
    Px=B*X'* inv(X*X'+lambdaE*eye(dX));
    Py=B*Y'* inv(Y*Y'+lambdaE*eye(dY));

    %objective function
    %J = lambdaX*norm(X-Ux*V,'fro')^2+lambdaY*norm(Y-Uy*V,'fro')^2+alpha*norm(L-G*V,'fro')^2+norm(B-R*V,'fro')^2+Xmu*norm(V-Wx*X,'fro')^2+Ymu*norm(V-Wy*Y,'fro')^2;
    %fprintf('objective function value @ iter %d: %f\n', iter, J);
end
